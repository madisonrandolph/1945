﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    // configuration parameters
    [Header("Movement Boundaries")]
    [SerializeField] float horizontalPadding = 1f;
    [SerializeField] float verticalPadding = 3.2f;

    [Header("Player Plane")]
    [SerializeField] float baseHorizontalSpeed = 5f;
    [SerializeField] float baseVerticalSpeed = 2f;
    [SerializeField] int startingHealth = 100;
    [SerializeField] int damageFromEnemyPlane = 30;
    [SerializeField] int damageFromEnemyProjectile = 10;
    [SerializeField] int respawnInvincibleLength = 8;
    [SerializeField] int damageInvincibleLength = 4;

    [Header("Projectile")]
    [SerializeField] GameObject playerBullet;
    [SerializeField] float standardBulletFireCooldown = .25f;
    [SerializeField] Vector2 bulletSpawnOffset = new Vector2(0, 0.1f);

    [Header("Powerup")]
    [SerializeField] float powerupHorizontalSpeed;
    [SerializeField] float powerupVerticalSpeed;
    [SerializeField] float powerupBulletFireCooldown = .125f;
    [SerializeField] int powerupProjectileQuantity = 3;
    [SerializeField] Vector2 powerupProjectileOffset = new Vector2(0.5f, 0);

    [Header("SFX/VFX")]
    [SerializeField] GameObject explosionVFX;
    [SerializeField] AudioClip explosionSound;
    [Range (0f, 1f)][SerializeField] float explosionVolume = 0.25f;
    [SerializeField] AudioClip powerupSound;
    [Range(0f, 1f)] [SerializeField] float powerupVolume = 0.25f;
    [SerializeField] AudioClip projectileSound;
    [Range(0f, 1f)] [SerializeField] float projectileVolume = 0.25f;
    [SerializeField] AudioClip bombSound;
    [Range(0f, 1f)] [SerializeField] float bombVolume = 0.25f;


    // state
    float cooldownTimer = 0;
    bool invincible = false;
    bool ammoPowerupActive = false;
    int health;
    float bulletFireCooldown;
    Vector3 projectileOffsetNudge;
    Vector3 powerupProjectileOffset3D;
    float horizontalSpeed;
    float verticalSpeed;

    Coroutine ammoPowerupRoutine;
    Coroutine powerPowerupRoutine;
    Coroutine dollarPowerupRoutine;
    Coroutine invincibleRoutine;

    float xMin;
    float xMax;
    float yMin;
    float yMax;

    // cached gameObjects
    Slider healthSlider;
    GameStatus gameStatus;

    // Use this for initialization
    void Start()
    {
        SetStartingStateVariables();
        CacheGameObjects();
        SetUpMoveBoundaries();
    }

    private void SetStartingStateVariables()
    {
        health = startingHealth;
        horizontalSpeed = baseHorizontalSpeed;
        verticalSpeed = baseVerticalSpeed;
        projectileOffsetNudge = new Vector3(bulletSpawnOffset.x, bulletSpawnOffset.y, 0);
        powerupProjectileOffset3D = new Vector3(powerupProjectileOffset.x, powerupProjectileOffset.y, 0);
        bulletFireCooldown = standardBulletFireCooldown;
    }

    private void CacheGameObjects()
    {
        healthSlider = FindObjectOfType<Slider>();
        gameStatus = FindObjectOfType<GameStatus>();
    }

    private void SetUpMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + horizontalPadding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - horizontalPadding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + verticalPadding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - verticalPadding;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        cooldownTimer -= Time.deltaTime;
        if (Input.GetAxis("Fire1") == 1 && cooldownTimer <= 0)
        {
            cooldownTimer = bulletFireCooldown;
            Fire();
        }
        if (Input.GetButtonDown("Fire2") && gameStatus.GetBombs() > 0)
        {
            ActivateBomb();
        }
    }

    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * horizontalSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * verticalSpeed;

        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);
        var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);
        transform.position = new Vector2(newXPos, newYPos);
    }

    private void Fire()
    {
        int numOfProjectiles;
        int iteration = 0;

        if (ammoPowerupActive)  {  numOfProjectiles = powerupProjectileQuantity;  }
        else  {  numOfProjectiles = 1;  }

        if (numOfProjectiles % 2 == 1) // numOfProjectiles is odd
        {
            Instantiate(playerBullet, (transform.position + projectileOffsetNudge), transform.rotation);
            for (int projectilesInstantiated = 1; projectilesInstantiated < numOfProjectiles; projectilesInstantiated += 2)
            {
                iteration++;
                Instantiate(playerBullet, (transform.position + projectileOffsetNudge - powerupProjectileOffset3D * iteration), transform.rotation);
                Instantiate(playerBullet, (transform.position + projectileOffsetNudge + powerupProjectileOffset3D * iteration), transform.rotation);
            }
        }
        else // numOfProjectiles is even
        {
            for (int projectilesInstantiated = 0; projectilesInstantiated < numOfProjectiles; projectilesInstantiated += 2)
            {
                iteration++;
                Instantiate(playerBullet, (transform.position + projectileOffsetNudge - powerupProjectileOffset3D * iteration + powerupProjectileOffset3D / 2f), transform.rotation);
                Instantiate(playerBullet, (transform.position + projectileOffsetNudge + powerupProjectileOffset3D * iteration - powerupProjectileOffset3D / 2f), transform.rotation);
            }
        }
        AudioSource.PlayClipAtPoint(projectileSound, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * projectileVolume));
    }

    private void ActivateBomb()
    {
        foreach (Enemy enemy in FindObjectsOfType<Enemy>())
        {
            enemy.BombPlane();
        }
        foreach (EnemyBullet bullet in FindObjectsOfType<EnemyBullet>())
        {
            Destroy(bullet.gameObject);
        }
        AudioSource.PlayClipAtPoint(bombSound, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * bombVolume));
        gameStatus.UseBomb();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!invincible)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                ApplyDamage(damageFromEnemyPlane);
            }
            if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy Projectile"))
            {
                ApplyDamage(damageFromEnemyProjectile);
            }
        }
    }

    private void ApplyDamage(int damage)
    {
        health -= damage;
        healthSlider.value = health;
        if (health <= 0)
        {
            AudioSource.PlayClipAtPoint(explosionSound, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * explosionVolume));
            TriggerExplosionVFX();
            healthSlider.value = startingHealth;
            FindObjectOfType<GameStatus>().LoseLife();
            Destroy(gameObject);
        }
        TriggerInvincible(damageInvincibleLength);
    }

    private void TriggerExplosionVFX()
    {
        Instantiate(explosionVFX, transform.position, transform.rotation);
    }

    public void TriggerInvincible(int invincibleLength)
    {
        if (invincibleRoutine != null) { StopCoroutine(invincibleRoutine); }
        invincibleRoutine = StartCoroutine(InvincibleVFX(invincibleLength));
    }
    
    private IEnumerator InvincibleVFX(int invincibleLength)
    {
        invincible = true;
        Renderer renderer = GetComponent<Renderer>();
        for (var n = 0; n < invincibleLength; n++)
        {
            renderer.enabled = true;
            yield return new WaitForSeconds(.1f);
            renderer.enabled = false;
            yield return new WaitForSeconds(.1f);
        }
        renderer.enabled = true;
        invincible = false;
    }

    public int getRespawnInvincibleLength()
    {
        return respawnInvincibleLength;
    }

    public void ApplyPowerup(String powerupType, int parameter)
    {
        if (powerupType == "Ammo")
        {
            ApplyAmmoPowerup(parameter);
        }
        else if (powerupType == "Repair")
        {
            ApplyRepairPowerup(parameter);
        }
        else if (powerupType == "Cross")
        {
            ApplyCrossPowerup(parameter);
        }
        else if (powerupType == "Power")
        {
            ApplyPowerPowerup(parameter);
        }
        else if (powerupType == "Dollar")
        {
            ApplyDollarPowerup(parameter);
        }
        else if (powerupType == "Star")
        {
            ApplyStarPowerup(parameter);
        }
        else
        {
            throw new NotImplementedException("Powerup type '" + powerupType + "' not a valid (implemented) powerup.");
        }
        AudioSource.PlayClipAtPoint(powerupSound, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * powerupVolume));
    }

    public void ApplyRepairPowerup(int repairAmount)
    {
        health = Mathf.Clamp(health + repairAmount, 0, startingHealth);
        healthSlider.value = health;
    }

    public void ApplyAmmoPowerup(int lengthOfTimeInSeconds)
    {
        if (ammoPowerupRoutine != null)  {  StopCoroutine(ammoPowerupRoutine);  }
        ammoPowerupRoutine = StartCoroutine(TriggerAmmoPowerup(lengthOfTimeInSeconds));
    }

    private IEnumerator TriggerAmmoPowerup(int lengthOfTimeInSeconds)
    {
        ammoPowerupActive = true;
        yield return new WaitForSeconds(lengthOfTimeInSeconds);
        ammoPowerupActive = false;
    }

    public void ApplyPowerPowerup(int lengthOfTimeInSeconds)
    {
        if (powerPowerupRoutine != null) { StopCoroutine(powerPowerupRoutine); }
        powerPowerupRoutine = StartCoroutine(TriggerPowerPowerup(lengthOfTimeInSeconds));
    }

    private IEnumerator TriggerPowerPowerup(int lengthOfTimeInSeconds)
    {
        bulletFireCooldown = powerupBulletFireCooldown;
        cooldownTimer = cooldownTimer - (bulletFireCooldown - powerupBulletFireCooldown);

        horizontalSpeed = powerupHorizontalSpeed;
        verticalSpeed = powerupVerticalSpeed;

        yield return new WaitForSeconds(lengthOfTimeInSeconds);

        bulletFireCooldown = standardBulletFireCooldown;

        horizontalSpeed = baseHorizontalSpeed;
        verticalSpeed = baseVerticalSpeed;
    }

    public void ApplyCrossPowerup(int lengthOfTimeInSeconds)
    {
        gameStatus.AddBomb();
    }

    public void ApplyDollarPowerup(int lengthOfTimeInSeconds)
    {
        gameStatus.ApplyDollarPowerup(lengthOfTimeInSeconds);
    }

    public void ApplyStarPowerup(int lengthOfTimeInSeconds)
    {
        TriggerInvincible(lengthOfTimeInSeconds);
    }
}
