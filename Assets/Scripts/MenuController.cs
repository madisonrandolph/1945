﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    // configuration parameters
    [SerializeField] GameObject menu;
    [SerializeField] AudioClip onPauseAudio;
    [SerializeField] AudioClip resumeAudio;
    [SerializeField] float menuAudioVolume = .25f;

    // variables
    bool menuActive = false;

    // cached references
    GameStatus gameStatus;

    private void Start()
    {
        gameStatus = FindObjectOfType<GameStatus>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel") && SceneManager.GetActiveScene().buildIndex == 1)
        {
            if (!gameStatus.isGameOver())
            {
                if (menuActive == false)
                {
                    DisplayMenu();
                }
                else
                {
                    CloseMenu();
                }
            }
        }
    }

    public void DisplayMenu()
    {
        AudioSource.PlayClipAtPoint(onPauseAudio, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * menuAudioVolume));
        FindObjectOfType<MusicPlayer>().GetComponent<AudioSource>().Pause();
        menuActive = true;
        menu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void CloseMenu()
    {
        menu.SetActive(false);
        Time.timeScale = 1f;
        AudioSource.PlayClipAtPoint(resumeAudio, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * menuAudioVolume));
        FindObjectOfType<MusicPlayer>().GetComponent<AudioSource>().UnPause();
        menuActive = false;
    }

    public void LoadNextScene()
    {
        CloseMenu();
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void LoadPreviousScene()
    {
        CloseMenu();
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex - 1);
    }

    public void ReloadScene()
    {
        CloseMenu();
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    public void LoadStartScene()
    {
        CloseMenu();
        SceneManager.LoadScene(0);
    }

    public void LoadInstructionsScene()
    {
        CloseMenu();
        SceneManager.LoadScene(2);
    }

    public void LoadSettingsScene()
    {
        CloseMenu();
        SceneManager.LoadScene(4);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
