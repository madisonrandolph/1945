﻿//using System;
using UnityEngine;

public class Enemy : MonoBehaviour {

    // configuration parameters
    [Header("Enemy Stats")]
    [SerializeField] float bottomOfGameScreenInUnits = 2.5f;
    [SerializeField] float moveSpeed = 3.0f;
    [SerializeField] bool startAtTop;
    [SerializeField] int pointsPerPlaneDestroyed = 5;

    [Header("Droppables")]
    [SerializeField] [Range(0f, 1f)] float powerupDropPercentChance = 0.05f;
    [SerializeField] GameObject[] droppablePowerups;
    [SerializeField] float powerupSpeed = 3f;

    [Header("Projectile")]
    [SerializeField] bool shootingEnemy;
    [SerializeField] bool shootingTrackingEnemy;
    [SerializeField] GameObject bullet;
    [SerializeField] float averageShootingDelay = 0.5f;
    [SerializeField] float shootingRange = 0.3f;

    [Header("VFX/SFX")]
    [SerializeField] GameObject explosionVFX;
    [SerializeField] AudioClip explosionSound;
    [Range(0f, 1f)] [SerializeField] float explosionVolume = 0.5f;

    // state
    float cooldownTimer;
    bool canFire = true;

    float xMin;
    float xMax;
    float yMin;
    float yMax;

    // Use this for initialization
    void Start () {
        SetUpScreenBoundaries();

        if (shootingEnemy || shootingTrackingEnemy)
        {
            ResetCooldown();
        }
	}

    private void SetUpScreenBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y;
    }

    // Update is called once per frame
    void Update ()
    {
        Move();
        CheckToFire();
    }

    private void Move()
    {
        if (startAtTop)
        {
            transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
        }
    }

    private void CheckToFire()
    {
        if (canFire && (shootingEnemy || shootingTrackingEnemy) && gameObject.transform.position.y > bottomOfGameScreenInUnits)
        {
            cooldownTimer -= Time.deltaTime;
            if (cooldownTimer <= 0)
            {
                Fire();
                ResetCooldown();
            }
        }
    }

    private void Fire()
    {
        Instantiate(bullet, transform.position, transform.rotation);
    }


    private void ResetCooldown()
    {
        cooldownTimer = Random.Range(averageShootingDelay - shootingRange, averageShootingDelay + shootingRange);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player Projectile"))
        {
            DropPowerup();
        }
        if (collision.gameObject.tag == "Boundary")
        {
            ResetPlane();
        }
        else
        {
            DestroyPlane();
        }
    }

    private void DestroyPlane()
    {
        FindObjectOfType<GameStatus>().AddToScore(pointsPerPlaneDestroyed);
        AudioSource.PlayClipAtPoint(explosionSound, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * explosionVolume));
        TriggerExplosionVFX();
        ResetPlane();
    }

    private void DropPowerup()
    {
        if (droppablePowerups.Length > 0)
        {
            if (Random.Range(0f, 1f) <= powerupDropPercentChance)
            {
                int powerupDroppedIndex = Random.Range(0, droppablePowerups.Length);
                var powerupDropped = Instantiate(droppablePowerups[powerupDroppedIndex], transform.position, Quaternion.identity);
                powerupDropped.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -powerupSpeed);
            }
        }
    }

    public void ResetPlane()
    {
        var newXPos = Random.Range(xMin, xMax);
        var newYPos = yMax + 1f;

        if (!startAtTop)
        {
            newYPos = -1f;
        }
        transform.position = new Vector2(newXPos, newYPos);

        GetComponent<Renderer>().enabled = true;
        gameObject.layer = LayerMask.NameToLayer("Enemy");
        canFire = true;
    }

    private void TriggerExplosionVFX()
    {
        Instantiate(explosionVFX, transform.position, transform.rotation);
    }

    public void BombPlane()
    {
        if (gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            TriggerExplosionVFX();
            GetComponent<Renderer>().enabled = false;
            gameObject.layer = LayerMask.NameToLayer("Disabled Enemy");
            canFire = false;
        }
    }
}
