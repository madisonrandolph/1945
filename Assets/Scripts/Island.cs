﻿using UnityEngine;

public class Island : MonoBehaviour {

    // configuration parameters
    [SerializeField] float moveSpeed = 3.0f;

    // state variables
    float xMin;
    float xMax;
    float yMin;
    float yMax;

    // Use this for initialization
    void Start()
    {
        SetUpScreenBoundaries();
        RandomizeStartXPos();
    }

    private void SetUpScreenBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y;
    }


    private void RandomizeStartXPos()
    {
        var newXPos = Random.Range(xMin, xMax);
        var yPos = transform.position.y;

        transform.position = new Vector2(newXPos, yPos);
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position.y <= yMin)
        {
            var newXPos = Random.Range(xMin, xMax);
            var newYPos = yMax + 1f;

            transform.position = new Vector2(newXPos, newYPos);
        }
        transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
    }


}
