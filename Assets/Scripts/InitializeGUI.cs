﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitializeGUI : MonoBehaviour {

    Slider[] sliders;
    Settings settings;

	// Use this for initialization
	void Start ()
    {
        CacheReferences();
        SetSliders();

        AddListeners();
    }

    private void AddListeners()
    {
        sliders[0].onValueChanged.AddListener(delegate { settings.SetMusicVolume(sliders[0].value); });
        sliders[0].onValueChanged.AddListener(delegate { FindObjectOfType<MusicPlayer>().SetMusicVolume(); });
        sliders[1].onValueChanged.AddListener(delegate { settings.SetSFXVolume(sliders[1].value); });
        FindObjectsOfType<Button>()[0].onClick.AddListener(delegate { SetSliders(); });
    }

    private void SetSliders()
    {
        sliders[0].value = settings.GetMusicVolume();
        sliders[1].value = settings.GetSFXVolume();
    }

    private void CacheReferences()
    {
        sliders = FindObjectsOfType<Slider>();
        settings = FindObjectOfType<Settings>();
    }
}
