﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class Scoreboard : MonoBehaviour {

    [SerializeField] TextMeshProUGUI[] nameTexts;
    [SerializeField] TextMeshProUGUI[] scoreTexts;

    Scores scores;
    String saveStatePath;
    bool isTypingName = false;
    TextMeshProUGUI nameText;
    int newScoreIndex = -1;


    // Use this for initialization
    void Start()
    {
        LoadScores();
    }

    private void LoadScores()
    {
        scores = new Scores();
        saveStatePath = Path.Combine(Application.persistentDataPath, "scoreboard.sgm");

        if (File.Exists(saveStatePath))
        {
            String fileContents = File.ReadAllText(saveStatePath);
            JsonUtility.FromJsonOverwrite(fileContents, scores);
        }
        for (int i = 0; i < scores.scores.Length; i++)
        {
            scoreTexts[i].text = scores.scores[i].ToString();
        }
        for (int i = 0; i < scores.names.Length; i++)
        {
            nameTexts[i].text = scores.names[i].ToString();
        }
    }

    void Update()
    {
        if (isTypingName)
        {
            if (nameText.text.Length == 0)
            {
                nameText.text = "_";
            }
            foreach (char c in Input.inputString)
            {
                if (c == '\b') // has backspace/delete been pressed?
                {
                    if (nameText.text.Length != 0)
                    {
                        nameText.text = nameText.text.Substring(0, nameText.text.Length - 2);
                        scores.names[newScoreIndex] = nameText.text;
                        nameText.text += "_";
                    }
                }
                else if ((c == '\n') || (c == '\r')) // enter/return
                {
                    isTypingName = false;
                    nameText.text = nameText.text.Substring(0, nameText.text.Length - 1);
                    CommitScores();
                }
                else
                {
                    nameText.text = nameText.text.Substring(0, nameText.text.Length - 1);
                    nameText.text += c;
                    scores.names[newScoreIndex] = nameText.text;
                    nameText.text += "_";
                }
            }
        }
    }

    internal void HandleScore(int finalScore)
    {
        int[] newScores = new int[scores.scores.Length];
        String[] newNames = new string[scores.names.Length];
        newScoreIndex = -1;
        for (int i = 0; i < scores.scores.Length; i++)
        {
            if (finalScore <= scores.scores[i]) // Keep the original score and name
            {
                newScores[i] = scores.scores[i];
                newNames[i] = scores.names[i];
            }
            else // Found the position to write the new score to
            {
                newScores[i] = finalScore;
                newNames[i] = "";
                newScoreIndex = i;
                break;
            }
        }
        if (newScoreIndex != -1) // The new score replaced a score in the array
        {
            for (int i = newScoreIndex + 1; i < scores.scores.Length; i++) // Move each of the remaining elements over one index
            {
                newScores[i] = scores.scores[i - 1];
                newNames[i] = scores.names[i - 1];
            }
        }
        scores.scores = newScores;
        scores.names = newNames;
        UpdateScoreboardUI();
        if (newScoreIndex != -1)
        {
            nameText = nameTexts[newScoreIndex];
            isTypingName = true;
        }
    }

    private void UpdateScoreboardUI()
    {
        for (int i = 0; i < scores.scores.Length; i++)
        {
            scoreTexts[i].text = scores.scores[i].ToString();
            nameTexts[i].text = scores.names[i];
        }
    }

    public void CommitScores()
    {
        File.WriteAllText(saveStatePath, JsonUtility.ToJson(scores, true));
    }

    void OnApplicationQuit()
    {
        CommitScores();
    }
}
