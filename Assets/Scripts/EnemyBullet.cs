﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    [SerializeField] float minMoveSpeed;
    [SerializeField] float maxMoveSpeed;
    [SerializeField] AudioClip explosionSound;
    [SerializeField] private Vector3 targetDirection;
    [Range(0f, 1f)] [SerializeField] float explosionVolume = 0.25f;

    // Use this for initialization
    void Start()
    {
        ApplyDirection();
    }

    private void ApplyDirection()
    {
        if (gameObject.tag == "TrackingBullet")
        {
            Player player = FindObjectOfType<Player>();
            if (player != null)
            {
                targetDirection = Vector3.Normalize(player.transform.position - gameObject.transform.position);     // Get the direction to the player
                float percentDown = 1 - Mathf.InverseLerp(-1, 1, targetDirection.y);                                // Identify how much "down-ness" the bullet travel path has
                float moveSpeed = Mathf.Lerp(minMoveSpeed, maxMoveSpeed, percentDown);                              // Adjust because bullets travelling up shouldn't go faster than ones going down
                targetDirection *= moveSpeed;
                gameObject.GetComponent<Rigidbody2D>().velocity = targetDirection;
            }
            else
            {
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -maxMoveSpeed);
            }
        }
        else
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -maxMoveSpeed);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(explosionSound, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * explosionVolume));
        }
        Destroy(gameObject);
    }
}