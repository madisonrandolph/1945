﻿using System;

public class Scores {

    public int[] scores = { 10000, 8000, 6000, 5000, 4000, 3000, 2000, 1000, 500, 250 };
    public String[] names = { "Dwight D. Eisenhower", "Winston Churchill", "Joseph Stalin", "Douglas MacArthur", "Charles de Gaulle", "Alan Brooke", "Georgy Zhukov", "George S. Patton", "Chiang Kai-shek", "Ivan Konev" };

}
