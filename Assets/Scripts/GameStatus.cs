﻿using System.Collections;
using UnityEngine;
using TMPro;

public class GameStatus : MonoBehaviour {

    // configuration parameters
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] Player player;
    [SerializeField] GameObject GameOverUI;

    [Header("Lives")]
    [SerializeField] GameObject life;
    [SerializeField] GameObject livesRemainingUI;
    [SerializeField] TextMeshProUGUI numLives;
    [SerializeField] Vector3 locationOfLifeIndicators = new Vector3(1.25f, 1.65f);
    [SerializeField] float lifeIndicatorsOffset = 1f;
    [SerializeField] int extraLives = 3;
    [SerializeField] int bonusLifeThreshold = 1000;
    [SerializeField] AudioClip bonusLifeAwardSound;
    [Range(0f, 1f)] [SerializeField] float bonusLifeAwardVolume = 0.25f;

    [Header("Bombs")]
    [SerializeField] GameObject bomb;
    [SerializeField] GameObject bombsRemainingUI;
    [SerializeField] TextMeshProUGUI numBombs;
    [SerializeField] Vector3 locationOfBombIndicators = new Vector3(13f, .65f);
    [SerializeField] float bombIndicatorsOffset = 1f;
    [SerializeField] int bombs = 3;

    // state variables
    int currentScore = 0;
    int bonusLivesAwarded = 0;
    bool isDoublePoints = false;
    GameObject[] lifeMarkers;
    GameObject[] bombMarkers;
    Coroutine dollarPowerupRoutine;
    bool gameOver = false;

    // Use this for initialization
    void Start () {
        scoreText.text = currentScore.ToString();
        SetLives();
        SetBombs();
    }

    private void SetLives()
    {
        lifeMarkers = new GameObject[extraLives];

        for (int i = 0; i < extraLives; i++)
        {
            Vector3 location = new Vector3(locationOfLifeIndicators.x + (lifeIndicatorsOffset * i), locationOfLifeIndicators.y, locationOfLifeIndicators.z);
            lifeMarkers[i] = Instantiate(life, location, transform.rotation);
        }
    }

    private void SetBombs()
    {
        bombMarkers = new GameObject[bombs];

        for (int i = 0; i < bombs; i++)
        {
            Vector3 location = new Vector3(locationOfBombIndicators.x + (bombIndicatorsOffset * i), locationOfBombIndicators.y, locationOfBombIndicators.z);
            bombMarkers[i] = Instantiate(bomb, location, transform.rotation);
        }
    }

    public bool isGameOver()
    {
        return gameOver;
    }

    public int GetBombs()
    {
        return bombs;
    }

    public void UseBomb()
    {
        if (bombs > 0)
        {
            bombs--;
            numBombs.text = bombs.ToString();
            if (bombs == 3)
            {
                Vector3 location = new Vector3(locationOfBombIndicators.x + (bombIndicatorsOffset * bombs - 1), locationOfBombIndicators.y, locationOfBombIndicators.z);
                bombMarkers[bombs - 1] = Instantiate(bomb, location, transform.rotation);
                location = new Vector3(locationOfBombIndicators.x + (bombIndicatorsOffset * (bombs - 2)), locationOfBombIndicators.y, locationOfBombIndicators.z);
                bombMarkers[bombs - 2] = Instantiate(bomb, location, transform.rotation);
                bombsRemainingUI.SetActive(false);
            }
            if (bombs < 3)
            {
                Destroy(bombMarkers[bombs]);
            }
        }
    }

    public void AddBomb()
    {
        if (bombs < 3)
        {
        Vector3 location = new Vector3(locationOfBombIndicators.x + (bombIndicatorsOffset * bombs), locationOfBombIndicators.y, locationOfBombIndicators.z);
        bombMarkers[bombs] = Instantiate(bomb, location, transform.rotation);
        }
        else if (bombs == 3)
        {
            Destroy(bombMarkers[1]);
            Destroy(bombMarkers[2]);
            bombsRemainingUI.SetActive(true);
        }
        bombs++;
        numBombs.text = bombs.ToString();
    }

    public void AddToScore(int score)
    {
        //var priorScore = currentScore;
        if (isDoublePoints)
        {
            currentScore = currentScore + score * 2;
        }
        else
        {
            currentScore += score;
        }
        if ((bonusLifeThreshold != 0) && (currentScore / bonusLifeThreshold > bonusLivesAwarded))
        {
            AddLife();
            bonusLivesAwarded++;
        }
        scoreText.text = currentScore.ToString();
    }

    public void LoseLife()
    {
        if (extraLives > 0)
        {
            extraLives--;
            numLives.text = extraLives.ToString();
            StartCoroutine("Respawn");

            if (extraLives == 3)
            {
                Vector3 location = new Vector3(locationOfLifeIndicators.x + (lifeIndicatorsOffset * extraLives - 1), locationOfLifeIndicators.y, locationOfLifeIndicators.z);
                lifeMarkers[extraLives - 1] = Instantiate(life, location, transform.rotation);
                location = new Vector3(locationOfLifeIndicators.x + (lifeIndicatorsOffset * (extraLives - 2)), locationOfLifeIndicators.y, locationOfLifeIndicators.z);
                lifeMarkers[extraLives - 2] = Instantiate(life, location, transform.rotation);
                livesRemainingUI.SetActive(false);
            }
            if (extraLives < 3)
            {
                Destroy(lifeMarkers[extraLives]);
            }
        }
        else
        {
            GameOver();
        }
    }

    private void AddLife()
    {
        AudioSource.PlayClipAtPoint(bonusLifeAwardSound, Camera.main.transform.position, (FindObjectOfType<Settings>().GetSFXVolume() * bonusLifeAwardVolume));
        if (extraLives < 3)
        {
            Vector3 location = new Vector3(locationOfLifeIndicators.x + (lifeIndicatorsOffset * extraLives), locationOfLifeIndicators.y, locationOfLifeIndicators.z);
            lifeMarkers[extraLives] = Instantiate(life, location, transform.rotation);
        }
        else if (extraLives == 3)
        {
            Destroy(lifeMarkers[1]);
            Destroy(lifeMarkers[2]);
            livesRemainingUI.SetActive(true);
        }
        extraLives++;
        numLives.text = extraLives.ToString();
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(2);
        var newPlayer = Instantiate(player, new Vector3(10, 4, 0), Quaternion.identity);
        newPlayer.TriggerInvincible(newPlayer.getRespawnInvincibleLength());
    }

    private void GameOver()
    {
        gameOver = true;
        GameOverUI.SetActive(true);
        FindObjectOfType<Scoreboard>().HandleScore(currentScore);
    }

    public void ApplyDollarPowerup(int lengthOfTimeInSeconds)
    {
        if (dollarPowerupRoutine != null) { StopCoroutine(dollarPowerupRoutine); }
        dollarPowerupRoutine = StartCoroutine(TriggerDollarPowerup(lengthOfTimeInSeconds));
    }

    private IEnumerator TriggerDollarPowerup(int lengthOfTimeInSeconds)
    {
        isDoublePoints = true;
        yield return new WaitForSeconds(lengthOfTimeInSeconds);
        isDoublePoints = false;
    }
}
