﻿using UnityEngine;

public class EnemyManager : MonoBehaviour {

    // configuration parameters
    [SerializeField] float firstEnemySpawnInSeconds = 7f;
    [SerializeField] Enemy[] enemies;
    [SerializeField] int[] enemySpawnDelayInSeconds;
    [SerializeField] int[] enemySpawnRateInSeconds;

    // internal variables
    float[] enemyCooldowns;
    float firstCooldown;
    bool firstDone = false;

    // Use this for initialization
    void Start ()
    {
        SpawnEnemy(enemies[0]);

        firstCooldown = firstEnemySpawnInSeconds;

        enemyCooldowns = new float[enemies.Length];
        for (int i = 0; i < enemies.Length; i++)
        {
            enemyCooldowns[i] = enemySpawnDelayInSeconds[i] + enemySpawnRateInSeconds[i];
        }
    }

    private void SpawnEnemy(Enemy enemy)
    {
        var plane = Instantiate(enemy);
        plane.ResetPlane();
    }

    // Update is called once per frame
    void Update ()
    {
        firstCooldown -= Time.deltaTime;
        for (int i = 0; i < enemies.Length; i++)
        {
            enemyCooldowns[i] -= Time.deltaTime;
        }

        if (firstCooldown <= 0 && !firstDone)
        {
            firstDone = true;
            SpawnEnemy(enemies[0]);
        }

        for (int i = 0; i < enemies.Length; i++)
        {
            if (enemyCooldowns[i] <= 0)
            {
                enemyCooldowns[i] = enemySpawnRateInSeconds[i];
                SpawnEnemy(enemies[i]);
            }
        }
    }
}
