﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField] float moveSpeed = 8f;

    private void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, moveSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
    }
}
