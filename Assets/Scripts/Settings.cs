﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour {

    public void SetMusicVolume(float volume)
    {
        PlayerPrefs.SetFloat("Music Volume", volume);
    }

    public float GetMusicVolume()
    {
        return PlayerPrefs.GetFloat("Music Volume", 1f);
    }

    public void SetSFXVolume(float volume)
    {
        PlayerPrefs.SetFloat("SFX Volume", volume);
    }

    public float GetSFXVolume()
    {
        return PlayerPrefs.GetFloat("SFX Volume", 1f);
    }

    public void SetAllToDefaults()
    {
        PlayerPrefs.DeleteAll();
    }

}
