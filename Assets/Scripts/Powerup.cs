﻿using System;
using UnityEngine;

public class Powerup : MonoBehaviour {

    [SerializeField] String powerupType;
    [SerializeField] int parameter = 20;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            collision.gameObject.GetComponent<Player>().ApplyPowerup(powerupType, parameter);
        }
    }
}
