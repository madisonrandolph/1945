﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    float musicVolume;

    // Use this for initialization
    void Awake()
    {
        SetUpSingleton();
    }

    private void SetUpSingleton()
    {
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        musicVolume = GetComponent<AudioSource>().volume;
        SetMusicVolume();
    }

    public void SetMusicVolume()
    {
        GetComponent<AudioSource>().volume = musicVolume * FindObjectOfType<Settings>().GetMusicVolume();
    }
}
